# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  def initialize(app)
    @app = app
  end

  def call(env)
    locale = extract_locale_from_header(env)
    if locale_is_available?(locale)
      Rails.logger.debug("== Locale was passed in header: #{locale} ==")
    else
      Rails.logger.debug("== Locale wasn't passed in header or not available. " \
                         "Setting default locale: #{I18n.default_locale} ==")
      locale = I18n.default_locale
    end
    I18n.locale = locale
    Rails.logger.debug("Locale was set to #{I18n.locale}")
    status, headers, body = @app.call(env)
    [status, headers, body]
  end

  def extract_locale_from_header(env)
    env['HTTP_ACCEPT_LANGUAGE']&.scan(/^[a-z]{2}/)&.first
  end

  def locale_is_available?(locale)
    locale && I18n.available_locales.include?(locale.to_sym)
  end
  # END
end
