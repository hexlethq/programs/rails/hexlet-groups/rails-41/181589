# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new(%w[a b])
  end

  def test_if_empty
    refute @stack.empty?
    @stack = Stack.new
    assert @stack.empty?
  end

  def test_size
    assert_equal @stack.size, 2
    @stack = Stack.new
    assert_equal @stack.size, 0
  end

  def test_to_a
    assert_equal @stack.to_a, %w[a b]
    @stack = Stack.new
    assert_equal @stack.to_a, []
  end

  def test_clearing
    assert_equal @stack.clear!, []
  end

  def test_pushing
    assert_equal @stack.push!('c'), %w[a b c]
  end

  def test_pop
    assert_equal @stack.pop!, 'b'
    assert_equal @stack.to_a, %w[a]
  end

  def test_pop_from_empty_stack
    @stack = Stack.new
    assert_nil @stack.pop!
    assert_equal @stack.to_a, []
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
