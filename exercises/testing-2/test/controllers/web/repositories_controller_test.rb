# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  test 'should create' do
    stub_request(:get, 'https://api.github.com/repos/octokit/octokit.rb')
      .to_return(body: load_fixture('files/response.json'), headers: { 'Content-Type' => 'json' })
    assert_difference 'Repository.count' do
      post repositories_path, params: { repository: { link: 'https://github.com/octokit/octokit.rb' } }
    end
  end
  # END
end
