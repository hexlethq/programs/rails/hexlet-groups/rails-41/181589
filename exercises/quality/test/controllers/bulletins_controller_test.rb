# frozen_string_literal: true

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  test 'can open all bulletins page' do
    get bulletins_path

    assert_response :success

    assert_select 'h1', 'Bulletins'
    assert_select 'li', 'Title 1'
    assert_select 'li', 'Title 2'
  end
  test 'can open single bulletin page' do
    get bulletin_path(bulletins(:bulletin1))

    assert_response :success
    assert_select 'h1', 'Title 1'
    assert_select 'p', 'Body 1'
  end
end
