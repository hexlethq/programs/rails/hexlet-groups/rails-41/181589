# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Status.create(name: 'New')
Status.create(name: 'In Progress')
Status.create(name: 'Done')

5.times do |index|
  user = User.create(name: "Human#{index}")
  Task.create(name: "Name#{index}", description: "Desc#{index}", user_id: user.id, status_id: Status.first.id)
end
