# frozen_string_literal: true

class Resume < ApplicationRecord
  validates :name, presence: true
  validates :contact, presence: true

  # BEGIN
  has_many :educations, class_name: 'ResumeEducation', dependent: :delete_all
  has_many :works, class_name: 'ResumeWork', dependent: :delete_all

  accepts_nested_attributes_for :educations, :works
  # END
end
