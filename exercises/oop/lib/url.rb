# frozen_string_literal: true

# BEGIN
require 'forwardable'
require 'uri'

class Url
  extend Forwardable
  extend URI
  include Comparable

  attr_reader :adress

  def_delegators :adress, :host, :scheme

  def initialize(adress)
    @adress = URI(adress)
  end

  def query_params
    adress.query.split('&').each_with_object({}) do |elem, object|
      pair = elem.split('=')
      object[pair[0].to_sym] = pair[1]
    end
  end

  def query_param(key, line = nil)
    query_params[key] || line
  end

  def <=>(other)
    adress <=> other.adress
  end
end
# END
