# frozen_string_literal: true

require 'test_helper'
require 'rake'

class HexletTest < ActiveSupport::TestCase
  def setup
    App::Application.load_tasks if Rake::Task.tasks.empty?
  end

  test 'import users' do
    test_dir_path = File.dirname(File.dirname(__FILE__))
    path = File.join test_dir_path, 'fixtures/files/users.csv'

    assert_difference('User.count', +100) do
      Rake::Task['hexlet:import_users'].invoke(path)
    end
  end
end
