require 'csv'

namespace :hexlet do
  desc 'Import users from csv file'
  task :import_users, [:path] => :environment do |_t, args|
    abort "Path to file wasn't passed" unless args[:path]
    abort "Path to file doesn't exist" unless File.exist?(args[:path])

    puts "Path to file: #{args[:path]}"
    data = CSV.read(args[:path], headers: true)
    puts "Records in file: #{data.count}"
    data.each do |row|
      User.create(row.to_h)
    end
    puts 'Users were imported'
  end
end
