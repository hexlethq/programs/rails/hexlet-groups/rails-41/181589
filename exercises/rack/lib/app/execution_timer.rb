# frozen_string_literal: true

class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    start_time = Time.now
    status, headers, body = @app.call(env)
    execution_time = (Time.now - start_time) * 1_000_000
    body << "\n#{execution_time.round}"
    [status, headers, body]
  end
end
