# frozen_string_literal: true

class Router
  def call(env)
    req = Rack::Request.new(env)
    if req.path.match('about')
      [200, {}, ['About page']]
    elsif req.path == '/'
      [200, {}, ['Hello, World!']]
    else
      [404, {}, ['404 Not Found']]
    end
  end
end
