require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test 'opens index page' do
    get tasks_path
    assert_response :success
    assert_select 'li', 'MyString1'
    assert_select 'li', 'MyString2'
  end

  test 'opens show page' do
    get task_path(tasks(:one))
    assert_response :success
    assert_select 'h1', "Task #{tasks(:one).id}"
    assert_select 'h2', 'MyString1'
  end

  test 'creates new task if validation passes' do
    get new_task_path
    assert_response :success

    post tasks_path, params: { task: {
      name: 'MyString3',
      description: 'MyText3',
      status: 'MyString3',
      creator: 'MyString3',
      performer: 'MyString3',
      completed: false
    } }
    assert_redirected_to task_path(Task.find_by(name: 'MyString3'))
    follow_redirect!
    assert_response :success
    assert_select 'h2', 'MyString3'
  end

  test 'displays errors on new page if validation fails' do
    get new_task_path
    assert_response :success

    post tasks_path, params: { task: { name: 'MyString3' } }
    assert_select 'div', "Creator can't be blank"
  end

  test 'edits existing task if validation passes' do
    get edit_task_path(tasks(:one))
    assert_response :success

    put task_path(tasks(:one)), params: { task: { name: 'NewString' } }
    assert_redirected_to task_path(tasks(:one))
    follow_redirect!
    assert_response :success
    assert_select 'h2', 'NewString'
  end

  test 'renders edit page again if validation fails' do
    get edit_task_path(tasks(:one))
    assert_response :success

    put task_path(tasks(:one)), params: { task: { name: nil } }
    assert_select 'div', "Name can't be blank"
  end

  test 'deletes existing task' do
    get task_path(tasks(:two))
    assert_response :success

    delete task_path(tasks(:two))
    assert_redirected_to tasks_path
    follow_redirect!
    assert_response :success
    assert_select 'li', { count: 0, text: 'MyString2' }
  end
end
