# frozen_string_literal: true

class RepositoryLoaderJob < ApplicationJob
  queue_as :default

  def perform(repository_id)
    repository = Repository.find(repository_id)
    repository.fetch
    repository.save
    client = Octokit::Client.new
    begin
      repo = client.repo Octokit::Repository.from_url(repository.link)
    rescue Octokit::NotFound
      repository.fetching_failed
    else
      repository.update(repo_info(repo))
      repository.fetching_succed
    end
    repository.save
  end

  private

  def repo_info(repo)
    {
      owner_name: repo[:owner][:login],
      repo_name: repo[:name],
      description: repo[:description],
      default_branch: repo[:default_branch],
      watchers_count: repo[:watchers_count],
      language: repo[:language],
      repo_created_at: repo[:created_at],
      repo_updated_at: repo[:updated_at]
    }
  end
end
