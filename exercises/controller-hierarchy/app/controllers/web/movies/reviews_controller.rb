# frozen_string_literal: true

module Web
  module Movies
    class ReviewsController < Web::Movies::ApplicationController
      def index
        @movie = Movie.find(params[:movie_id])
        @reviews = @movie.reviews
      end

      def new
        @review = Review.new
      end

      def create
        @movie = Movie.find(params[:movie_id])
        @review = @movie.reviews.build(review_params)

        if @review.save
          redirect_to movie_reviews_path(@movie), notice: t('success')
        else
          render :new, notice: t('fail')
        end
      end

      def edit
        @review = Review.find(params[:id])
      end

      def update
        @review = Review.find(params[:id])

        if @review.update(review_params)
          redirect_to movie_reviews_path(@review.movie), notice: t('success')
        else
          render :edit, notice: t('fail')
        end
      end

      def destroy
        @review = Review.find(params[:id])

        if @review.destroy
          redirect_to movie_reviews_path(@review.movie), notice: t('success')
        else
          redirect_to movie_reviews_path(@review.movie), notice: t('fail')
        end
      end

      private

      def review_params
        params.require(:review).permit(:body)
      end
    end
  end
end
