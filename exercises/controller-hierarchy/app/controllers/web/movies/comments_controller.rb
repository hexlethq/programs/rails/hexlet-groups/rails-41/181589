# frozen_string_literal: true

module Web
  module Movies
    class CommentsController < Web::Movies::ApplicationController
      def index
        @movie = Movie.find(params[:movie_id])
        @comments = @movie.comments
      end

      def new
        @comment = Comment.new
      end

      def create
        @movie = Movie.find(params[:movie_id])
        @comment = @movie.comments.build(comment_params)

        if @comment.save
          redirect_to movie_comments_path(@movie), notice: t('success')
        else
          render :new, notice: t('fail')
        end
      end

      def edit
        @comment = Comment.find(params[:id])
      end

      def update
        @comment = Comment.find(params[:id])

        if @comment.update(comment_params)
          redirect_to movie_comments_path(@comment.movie), notice: t('success')
        else
          render :edit, notice: t('fail')
        end
      end

      def destroy
        @comment = Comment.find(params[:id])

        if @comment.destroy
          redirect_to movie_comments_path(@comment.movie), notice: t('success')
        else
          redirect_to movie_comments_path(@comment.movie), notice: t('fail')
        end
      end

      private

      def comment_params
        params.require(:comment).permit(:body)
      end
    end
  end
end
