# frozen_string_literal: true

require 'application_system_test_case'
class Post
  class CommentsTest < ApplicationSystemTestCase
    test 'showing post comments' do
      visit post_path(posts(:one))

      assert_selector 'small', text: post_comments(:one).body
    end

    test 'creating new comment' do
      visit post_path(posts(:without_comments))

      fill_in 'post_comment[body]', with: post_comments(:one).body
      click_on 'Create Comment'
      assert_text 'Comment was successfully created.'
      assert_selector 'small', text: post_comments(:one).body
    end

    test 'updating comment' do
      visit post_path(posts(:one))

      click_link 'Edit', match: :first
      fill_in 'Body', with: 'New'
      click_on 'Update Comment'
      assert_text 'Comment was successfully updated'
      assert_selector 'small', text: 'New'
    end

    test 'destroying comment' do
      visit post_path(posts(:two))

      page.accept_confirm do
        click_on 'Delete'
      end
      refute page.has_css? 'card mb-2 p-3'
    end
  end
end
