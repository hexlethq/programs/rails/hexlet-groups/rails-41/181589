# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  test 'visiting the index' do
    visit posts_path

    assert_selector 'h1', text: 'Posts'
  end

  test 'visiting show page' do
    visit post_path(posts(:one))

    assert_selector 'h1', text: posts(:one).title
    assert_selector 'p', text: posts(:one).body
  end
  test 'creating new post' do
    visit posts_path

    click_on 'New Post'
    fill_in 'Title', with: posts(:one).title
    fill_in 'Body', with: posts(:one).body
    click_on 'Create Post'
    assert_text 'Post was successfully created'
  end

  test 'updating post' do
    visit posts_path

    click_on 'Edit', match: :first
    fill_in 'Title', with: posts(:two).title
    click_on 'Update Post'
    assert_text 'Post was successfully updated'
    assert_selector 'h1', text: posts(:two).title
  end

  test 'destroying post' do
    visit posts_path

    page.accept_confirm do
      click_on 'Destroy', match: :first
    end
    assert_text 'Post was successfully destroyed'
    assert_selector 'td', count: 4
  end
end
# END
