# frozen_string_literal: true

# BEGIN
def fibonacci(num, current_num = 0, next_num = 1)
  if num.negative?
    nil
  elsif num == 1
    current_num
  else
    fibonacci(num - 1, next_num, next_num + current_num)
  end
end
# END
