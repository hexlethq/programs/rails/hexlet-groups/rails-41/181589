# frozen_string_literal: true

# BEGIN
def reverse(line)
  counter = 0
  new_line = ''
  while counter < line.size
    new_line = "#{line[counter]}#{new_line}"
    counter += 1
  end
  new_line
end
# END
