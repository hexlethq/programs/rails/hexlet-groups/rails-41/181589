# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  elems = (start..stop).map do |elem|
    if (elem % 3).zero? || (elem % 5).zero?
      line = ''
      line += 'Fizz' if (elem % 3).zero?
      line += 'Buzz' if (elem % 5).zero?
      line
    else
      elem
    end
  end
  elems.join(' ')
end
# END
