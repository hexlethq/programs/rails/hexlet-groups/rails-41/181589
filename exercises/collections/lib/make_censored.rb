# frozen_string_literal: true

# rubocop:disable Style/For

def make_censored(text, stop_words)
  # BEGIN
  words = text.split
  words.each_with_index do |word, index|
    words[index] = '$#%!' if stop_words.include? word
  end
  words.join(' ')
  # END
end

# rubocop:enable Style/For
