# frozen_string_literal: true

# rubocop:disable Style/For
# BEGIN
def build_query_string(hash)
  line = []
  hash.to_a.sort.each do |pair|
    line << "#{pair[0]}=#{pair[1]}"
  end
  line.join('&')
end
# END
# rubocop:enable Style/For
