# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  version1 = version1.split('.').map(&:to_i)
  version2 = version2.split('.').map(&:to_i)
  version1 <=> version2
end
# END
