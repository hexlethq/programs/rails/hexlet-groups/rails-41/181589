# frozen_string_literal: true

# BEGIN
def get_same_parity(array)
  if array.first&.even?
    array.filter(&:even?)
  else
    array.filter(&:odd?)
  end
end
# END
