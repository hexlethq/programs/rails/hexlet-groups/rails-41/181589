# frozen_string_literal: true

# BEGIN
def word_to_hash(word)
  word.each_char.with_object({}) do |char, object|
    object[char] = word.count(char)
  end
end

def anagramm_filter(word, array)
  word_hash = word_to_hash(word)
  array.select { |elem| word_hash == word_to_hash(elem) }
end
# END
