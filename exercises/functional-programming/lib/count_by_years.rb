# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  users.each_with_object({}) do |hash, object|
    next unless hash[:gender] == 'male'

    year = hash[:birthday].split('-')
    object[year.first] ||= 0
    object[year.first] += 1
  end
end
# END
