# frozen_string_literal: true

# BEGIN
require 'date'

module Model
  def self.included(class_name)
    class_name.extend(AttributeMethods)
  end

  def self.add_attributes(key, value)
    @@attributes ||= {}
    @@attributes[key] = value
  end

  module AttributeMethods
    def attribute(name, options = {})
      define_method name do
        instance_variable_get "@#{name}"
      end
      define_method "#{name}=" do |value|
        value = convert_attr(options[:type], value)
        instance_variable_set "@#{name}", value
      end
      Model.add_attributes(name, options[:type])
    end
  end

  def attributes
    @@attributes.keys.each_with_object({}) do |attr, object|
      variable = "@#{attr}".to_sym
      object[attr] = instance_variable_get(variable)
    end
  end

  def convert_attr(type, value)
    bool_value = { 'yes' => true, 'no' => false }
    case type
    when :integer then value ? value.to_i : value
    when :string then value.to_s
    when :datetime then DateTime.parse(value)
    when :boolean
      bool_value[value] || value
    end
  end

  def initialize(hash = {})
    hash.each do |key, value|
      value = convert_attr(@@attributes[key], value)
      instance_variable_set "@#{key}", value
    end
  end
end
# END
