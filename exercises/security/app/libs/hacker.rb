# frozen_string_literal: true

require 'open-uri'
require 'nokogiri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      host_name = 'https://rails-l4-collective-blog.herokuapp.com'
      reg_page_path = '/users/sign_up'
      reg_action_path = '/users'
      page = URI.parse("#{host_name}#{reg_page_path}").open
      html_document = Nokogiri::HTML(page)
      token = html_document.at('input[type="hidden"]')['value']
      cookie = page.meta['set-cookie']
      uri = URI.parse("#{host_name}#{reg_action_path}")
      params = { authenticity_token: token, email: email, password: password, password_confirmation: password }
      headers = { 'Cookie' => cookie }
      Net::HTTP.post(uri, params.to_query, headers)
      # END
    end
  end
end
